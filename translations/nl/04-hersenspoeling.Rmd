# Hersenspoeling

Dit is de tweede reden waarom we beginnen met gebruiken. Om deze hersenspoeling volledig te begrijpen, moesten we eerst de krachtige effecten van supernormale stimuli onderzoeken. Onze hersenen zijn eenvoudigweg niet voorbereid op het creëren van een 'online harem', waardoor we tussen meer potentiële partners kunnen schakelen in vijftien minuten dan onze voorouders in meerdere levens hadden.

Er is veel verkeerd advies geweest in het verleden, een voorbeeld daarvan is dat masturbatie leidt tot blindheid. Dit, samen met andere angsttactieken, overdreef duidelijk. Misvattingen zoals deze waren terecht omvergeworpen door de wetenschap. Maar het kind is met het badwater weggegooid; vanaf onze vroegste jaren worden onze onderbewuste geesten overspoeld met seksuele boodschappen en beelden, tijdschriften en advertenties vol met dubbelzinnigheden. Sommige popvideo's zijn uiterst suggestief, maar raak niet in wanhoop, maak er een spel van om te identificeren welke componenten ze gebruiken - is het shockwaarde, nieuwheid, kleur, grootte, taboe, nostalgie, enzovoort. Zo'n spel kan zelfs aan pre-tieners worden geleerd als een manier om hen te educeren.

De kernboodschap is: *"Het meest waardevolle op deze aarde, mijn laatste gedachte en handeling, zal een orgasme zijn."* Is dit een overdrijving? Kijk naar elk TV- of filmverhaal en je zult de vermenging van de zintuiglijke (aanraking, geur, stem) en de voortplantingsgerichte (orgasmische) delen van seks zien. De invloed hiervan dringt niet door tot ons bewustzijn, maar het onderbewustzijn neemt het wel op.

## Wetenschappelijke redenering

Er is publiciteit in de andere richting: angsten voor seksuele disfunctie, verlies van motivatie, de voorkeur geven aan virtuele porno boven echte meisjes, YourBrainOnPorn.com en verschillende internet-subculturen, maar deze bewegingen weerhouden mensen er eigenlijk niet van om te blijven gebruiken. Logischerwijs zouden ze dat wel moeten doen, maar het simpele feit is dat ze dat niet doen. Zelfs de gezondheidsrisico's die vermeld worden in de studies op YourBrainOnPorn.com zijn niet genoeg om een adolescent ervan te weerhouden ermee te beginnen.

Ironisch genoeg is de krachtigste kracht in deze verwarring de gebruiker zelf. Het is een misvatting dat gebruikers zwak-wilskrachtig of fysiek zwakke mensen zijn. Je moet fysiek sterk zijn om met een verslaving om te kunnen gaan nadat je weet dat deze bestaat. Misschien is het meest pijnlijke aspect dat ze zichzelf neerzetten als mislukkelingen en onverdraagzame introverten. Het is waarschijnlijk dat een vriend interessanter zou zijn in persoon als ze zichzelf niet naar beneden hadden gehaald voor het zoeken naar zelfgenoegen.
![The Pornography Trap](images/trap.png)

## Problemen bij het gebruiken van wilskracht

Gebruikers die proberen te stoppen met behulp van de wilskrachtmethode geven vaak hun eigen gebrek aan wilskracht de schuld en verpesten daarmee hun rust en geluk. Het is één ding om te falen in zelfdiscipline en iets anders om zelfhaat te voelen. Uiteindelijk is er geen wet die vereist dat je altijd hard moet zijn vóór seks, goed opgewonden moet zijn en in staat moet zijn om je partner te bevredigen. We werken aan een verslaving, niet aan een gewoonte, en op geen enkel moment hoef je met jezelf te discussiëren om een gewoonte zoals golfen te stoppen, maar om hetzelfde te doen met een pornoverslaving wordt genormaliseerd — waarom?

Constante blootstelling aan een bovennatuurlijke prikkel verandert je brein, dus het opbouwen van weerstand tegen deze hersenspoeling is belangrijk, alsof je een auto koopt van een tweedehands autodealer — beleefd knikken maar geen woord geloven van wat de man zegt. Geloof dus niet dat je zoveel mogelijk seks moet hebben, allemaal uitzonderlijk goed met porno als het niet beschikbaar is.

Speel ook niet het veilige pornospel; je kleine monster heeft dat spel bedacht om je te lokken. Is amateurporno gecertificeerd door een of andere autoriteit? Pornowebsites verzamelen gegevens van hun gebruikers en gebruiken deze om aan hun behoeften te voldoen, en als ze een stijging zien in een bepaalde categorie, zullen ze zich daarop richten en zo snel mogelijk inhoud leveren. Laat je niet misleiden door educatieve bedoelingen of 'veilige' clips die op vrouwen gericht zijn. Begin jezelf af te vragen: *"Waarom doe ik dit? Heb ik dit echt nodig?"*

** Nee, natuurlijk niet!**

De meeste gebruikers zweren dat ze alleen statische en zachte porno kijken en daarom prima zijn, terwijl ze in werkelijkheid aan de lijn trekken, vechtend met hun wilskracht om verleidingen te weerstaan. Als dit te vaak en te lang wordt gedaan, put dit hun wilskracht aanzienlijk uit en beginnen ze te falen in andere levensprojecten waar wilskracht van grote waarde is, zoals sporten, diëten, enz. Falen op deze gebieden maakt hen ellendig en schuldig, wat zich weer vertaalt naar het opnieuw gebruiken van porno. Als dit niet gebeurt, uiten ze hun woede en depressie op hun geliefden.

Eenmaal verslaafd aan internetporno, wordt de hersenspoeling versterkt. Je onderbewuste weet dat het kleine monster gevoed moet worden en blokkeert alles wat daar tegenin gaat. Het is angst die mensen ervan weerhoudt om te stoppen, angst voor dat lege, onzekere gevoel dat ze krijgen wanneer ze stoppen met hun hersenen te overspoelen met dopamine. Alleen omdat je er niet van bewust bent, betekent niet dat het er niet is. Je hoeft het niet te begrijpen, net zoals een kat niet hoeft te begrijpen waar de hete waterleidingen zijn: de kat weet gewoon dat als hij op een bepaalde plek gaat zitten, hij warmte voelt.

## Passiviteit

De passiviteit van onze geesten en onze afhankelijkheid van autoriteit die leidt tot hersenspoeling, vormen de voornaamste moeilijkheid bij het stoppen met porno. Onze opvoeding in de samenleving, versterkt door de hersenspoeling van onze eigen verslaving en gecombineerd met de krachtigste - onze vrienden, familieleden en collega's. De frase 'opgeven' is een klassiek voorbeeld van hersenspoeling, waarbij een echt offer wordt gesuggereerd. De prachtige waarheid is dat er niets op te geven valt; integendeel, je zult jezelf bevrijden van een vreselijke ziekte en prachtige positieve resultaten behalen. We zullen deze hersenspoeling nu beginnen te verwijderen, te beginnen met niet langer te verwijzen naar 'opgeven', maar naar stoppen, ermee ophouden, of misschien wel de ware positie, **ontsnappen!**

Het enige dat ons in eerste instantie overtuigt om te gebruiken, is dat anderen het doen en het gevoel hebben dat we iets missen. We werken hard om verslaafd te raken, maar vinden nooit wat zij missen. Elke keer dat we een andere clip zien, verzekert het ons dat er wel iets in moet zitten, anders zouden mensen het niet doen en zou de business niet zo groot zijn. Zelfs als ze van de gewoonte afkomen, voelt de ex-gebruiker zich beroofd wanneer er tijdens feestjes of sociale functies een discussie ontstaat over een sexy entertainer, zanger of zelfs een pornoster. *"Ze moeten wel goed zijn als al mijn vrienden over hen praten, toch? Hebben ze gratis foto's online?"* Ze voelen zich veilig, ze zullen vanavond gewoon even kijken en voor ze het weten, zijn ze weer verslaafd.

De hersenspoeling is buitengewoon krachtig en je moet je bewust zijn van de effecten ervan. Technologie blijft groeien en de toekomst zal exponentieel snellere sites en toegangsmethoden brengen. De pornografie-industrie investeert miljoenen in virtual reality, zodat het de volgende grote trend zal worden. We weten niet waar we naartoe gaan, zijn niet uitgerust om met de huidige technologie om te gaan of met wat er nog gaat komen.

We staan op het punt om deze hersenspoeling te verwijderen. Het is niet de niet-gebruiker die wordt beroofd, maar de gebruiker die een leven lang opgeeft:

-   Gezondheid

-   Energie

-   Rijkdom

-   Rust in het hoofd

-   Zelfverzekerheid

-   Moed

-   Zelf-respect

-   Blijheid

-   Vrijheid

Wat bereiken ze met deze aanzienlijke opofferingen? **ABSOLUUT NIETS**, behalve de illusie van proberen terug te keren naar de staat van vrede, rust en vertrouwen die de niet-gebruiker altijd geniet.

## Afkickverschijnselen

Zoals eerder uitgelegd, geloven gebruikers dat ze porno gebruiken voor plezier, ontspanning of soortgelijke educatieve doeleinden. De werkelijke reden is echter verlichting van afkickverschijnselen. Ons onderbewustzijn begint te leren dat internetporno en masturbatie op bepaalde tijdstippen plezierig kunnen zijn. Naarmate we steeds meer verslaafd raken aan de drug, neemt de behoefte om de afkickverschijnselen te verlichten toe en sleept het subtiele valstrik je steeds verder naar beneden. Dit proces verloopt zo langzaam dat je je er niet eens van bewust bent; de meeste jonge gebruikers realiseren zich niet dat ze verslaafd zijn totdat ze proberen te stoppen en zelfs dan zullen velen het niet toegeven.

Neem deze conversatie die een therapeut had met honderden tieners:"

>**Therapeut:** “*Je beseft dat internetporno een drug is en de enige reden waarom je het gebruikt is omdat je niet kunt stoppen.*”
>
>**Patiënt:** “*Onzin! Ik geniet ervan, als dat niet zo was, zou ik stoppen.*”
>
>**Therapeut:** “*Stop gewoon een week om me te bewijzen dat je kunt stoppen als je wilt.*”
>
>**Patiënt:** “*Prima, ik geniet ervan. Als ik zou willen stoppen, zou ik dat doen.*” 
>
>**Therapeut:** “*Stop gewoon een week om aan jezelf te bewijzen dat je niet verslaafd bent.*”
>
>**Patiënt:** “*Wat is het nut? Ik geniet ervan.”*

Zoals al eerder vermeld, hebben gebruikers de neiging om hun afkickverschijnselen te verlichten op momenten van stress, verveling, concentratie of combinaties daarvan. In de volgende hoofdstukken zullen we ons richten op deze aspecten van de hersenspoeling.

# Gezondheid

Dit is het gebied waar de hersenspoeling het grootst is bij gebruikers — vooral de jonge en alleenstaande gebruikers — die denken dat ze op de hoogte zijn van de gezondheidsrisico's, maar dat niet zijn. Velen houden zichzelf voor de gek door te zeggen dat ze bereid zijn de gevolgen te accepteren. Als je internetrouter een functie had die een alarmsignaal afspeelde met een waarschuwing wanneer je een pornosite bezocht, en zei: "*Tot nu toe ben je ermee weggekomen, maar als je nog een minuut blijft, zal je hoofd ontploffen,*" zou je dan zijn gebleven? Als je twijfelt over het antwoord, probeer dan naar een klif te lopen, op de rand te staan met je ogen dicht en je voor te stellen dat je de keuze hebt tussen stoppen met porno of geblinddoekt naar voren lopen.

Er bestaat geen twijfel over wat je keuze zou zijn, maar door je kop in het zand te steken en te hopen dat je op een ochtend wakker wordt en geen zin meer hebt om naar porno te kijken, bereik je niets. Gebruikers kunnen zichzelf niet toestaan te denken aan de gezondheidsrisico's, want als ze dat doen, verdwijnt het illusoire genot van de verslaving. Dit verklaart waarom schokbehandelingen zo ineffectief zijn in de eerste fase van stoppen: alleen niet-gebruikers halen het in zichzelf om te lezen over de destructieve veranderingen in de hersenen.

Bijvoorbeeld dit gesprek met gebruikers, vaak jongeren:

> **Ik:** “Waarom wil je stoppen?"
>
> **Gebruiker:** “*Ik las in een blog van een versierder dat het goed is om vier dagen te stoppen om mezelf op te peppen.*"
>
> **Ik:** “Maak je je geen zorgen om de gezondheidrisico’s?"
>
> **Gebruiker:** “*Nee, want ik kan onder de bus belanden morgen.*"
>
> **Ik:** “Maar zou je opzettelijk onder een bus stappen?"
>
> **Gebruiker:** “*Natuurlijk niet*"  
>
> **Ik:** “Kijk je niet naar links en rechts als je de weg oversteekt?"
>
> **Gebruiker:** “*Natuurlijk wel.*"

Precies, ze doen veel moeite om niet onder een bus te stappen en de kans daarop is duizenden tegen één. Toch riskeert de gebruiker de bijna zekerheid om beschadigd te worden door hun verslaving en lijkt volledig onbewust te zijn. Zo krachtig is de hersenspoeling; internetporno is een wolf in schaapskleren. Is het niet vreemd dat als we het minste foutje in een vliegtuig voelen, we er niet mee zouden vliegen — zelfs als de risico's miljoenen tegen één zijn — maar we meer dan een kans van één op vier met porno nemen en blijkbaar onbewust zijn? Wat heeft de gebruiker hieraan? **Absoluut niets!**

Een andere veelvoorkomende mythe is depressie of prikkelbaarheid. Veel jongere mensen maken zich geen zorgen over hun gezondheid omdat ze geen last hebben van depressie of neerslachtigheid. De depressie of stress is niet de ziekte, het is een symptoom. Jongere mensen voelen over het algemeen geen prikkelbaarheid of depressie als gevolg van hun lichaams natuurlijke vermogen om meer dopamine aan te maken. Naarmate ze ouder worden of ernstige tegenslagen in hun leven ervaren, raken hun toch al uitgeputte hulpbronnen overbelast en zullen ze volledige symptomen ervaren. Wanneer oudere gebruikers zich gestrest, depressief of geïrriteerd voelen, komt dit doordat de fail-safe mechanismen van de natuur het zenuwstelsel beschermen tegen overmatige dopamine-overspoeling door receptoren bij te snijden. De gebruiker ontwikkelt ook andere neurologische veranderingen die hen in de sleur houden.

Bekijk het op deze manier: als je een mooie auto hebt en deze laat roesten zonder er iets aan te doen, zou dat behoorlijk dom zijn. Het zou al snel een onbeweeglijke hoop roest worden, niet in staat om je ergens naartoe te brengen. Het zou echter niet het einde van de wereld zijn, omdat het slechts een kwestie van geld is. Maar je lichaam is het voertuig dat je door het leven draagt. We zeggen allemaal dat onze gezondheid ons grootste bezit is — vraag het maar aan een zieke miljonair. De meesten van ons kunnen terugkijken op een ziekte of ongeval in ons leven waarbij we baden om beter te worden. Door een pornogebruiker te zijn, laat je niet alleen de roest binnenkomen en doe je er niets aan, je vernietigt systematisch het enige voertuig dat wordt gebruikt om door je hele leven te gaan.

Word wijzer. Je hoeft dit niet te doen. Onthoud, het levert *absoluut niets voor je op*. Neem heel even je hoofd uit het zand en vraag jezelf af: als je met zekerheid wist dat je volgende sessie een proces zou starten waardoor je volledig ongevoelig zou worden voor iemand van wie je diep houdt, zou je dan blijven gebruiken? Als je met deze mensen zou praten, zouden ze zeker niet verwachten dat het hen zou overkomen, en het ergste is niet de ziekte zelf, maar het besef dat ze het aan zichzelf te wijten hebben. Probeer je voor te stellen hoe mensen die 'op de knop hebben gedrukt' zich voelen, voor hen is de hersenspoeling voorbij. Ze spenderen de rest van hun leven denkend: "*Waarom heb ik mezelf zo lang wijsgemaakt dat ik moest masturberen naar internetporno? Als ik maar de kans had om terug te gaan!*"

Hou op met jezelf voor de gek te houden, je hebt die kans. Het is een kettingreactie: als je betrokken raakt bij de volgende pornosessie, leidt het tot de volgende en de volgende. Het gebeurt al met je. EasyPeasy belooft geen schokkende behandeling, dus als je al hebt besloten dat je gaat stoppen, zal het volgende niet schokkend voor je zijn. Als je dat niet hebt, sla dan het overgebleven deel van dit hoofdstuk over en kom erop terug nadat je de rest van het boek hebt gelezen.

Er zijn al talloze onderzoeken geschreven over de schade die internetporno aanricht aan ons seksleven en onze mentale gezondheid. Het probleem is dat mensen tot ze besluiten te stoppen, het niet willen weten. Forums en mentor groepen zijn tijdverspilling omdat porno de blinddoeken omdoet. Als ze er per ongeluk toch over lezen, is het eerste wat ze doen hun favoriete tube-site openen. Pornogebruikers denken vaak dat geluk, stress en seksuele risico's een kwestie van geluk zijn, als het betreden van een mijnenveld.

Laat het tot je doordringen, het gebeurt al. ***Elke keer*** dat je je pornowebsite opent, activeer je een stroom van dopamine en beginnen de opioïden te werken. De neurale glijbanen zijn gesmeerd en de rit voert je soepel door de volgende stappen, je brein heeft zich al overgegeven aan het script. Het zenuwstelsel wordt nu overspoeld door dopamine en omdat het niet de eerste keer is, sluiten dopamine-receptoren zich en gebruikt het kleine monster deze lichte daling in plezier ten opzichte van de vorige keer om je verder over de rode lijn te duwen naar meer schokkende porno of gedrag om meer dopamine vrij te geven. Meer nieuwigheid, meer dopamine en het kleine monster zegt je om door te gaan. Zo veel afbeeldingen en video's in één sessie zorgen voor een supernormale prikkel, die meer chemicaliën in de hersenen injecteert en je aanzet om door te gaan.

De hele tijd ontvangen je receptoren informatie om zich af te sluiten in reactie op de overstroming. Een orgasme versterkt alleen dit effect en leidt tot ontwenning. Je bent in ontkenning omdat het kleine monster verlangt naar zijn fix zonder echte pijn en ongemak. De dreiging van erectiestoornissen jaagt velen angst aan, daarom blokkeren ze het uit hun gedachten en overschaduwen het met de angst om te stoppen. Het is niet dat de angst groter is, maar stoppen vandaag is direct. Waarom naar de negatieve kant kijken? Misschien gebeurt het niet, aangezien je tegen die tijd toch al gestopt zou zijn.

We neigen ernaar om pornografie te zien als een strijd: aan de ene kant is er angst, "*Het is ongezond, vies en slavend.*" Aan de andere kant de positieve kant: "*Het is mijn plezier, mijn vriend, mijn steun.*" Het lijkt nooit tot ons door te dringen dat deze kant ook angst is; het is niet dat we genieten van porno, het is dat we vaak ongelukkig zijn zonder het. Heroïneverslaafden die van heroïne worden beroofd, ervaren ongeluk, maar stel je de complete vreugde voor als ze eindelijk toegestaan worden een naald in hun ader te steken en die vreselijke drang te beëindigen. Probeer je voor te stellen hoe iemand eigenlijk zou kunnen geloven dat ze genot halen uit het inbrengen van een injectienaald in een ader. Niet-heroïneverslaafden ervaren dat paniekerige gevoel niet en heroïne verlicht het gevoel niet, het veroorzaakt het.
Niet-gebruikers voelen zich niet ongelukkig als ze geen toegang hebben tot porno - alleen gebruikers ervaren dat gevoel. Internetporno verlicht dat gevoel niet, het veroorzaakt het. De angst voor de negatieve gevolgen helpt gebruikers niet om te stoppen, omdat ze het gevoel vergelijken met het lopen door een mijnenveld. Als je ermee wegkomt, prima, maar als je pech had en op een mijn stapte, stond je voor de gevolgen. Als je de risico's kende en bereid was ze te nemen, wat had dat dan met iemand anders te maken? Verslaafden in deze staat ontwikkelen typisch de volgende ontwijkende tactieken.

“***Je zult uiteindelijk toch oud worden en je seksuele kracht verliezen....***"  

Natuurlijk wel, maar seksuele kracht is niet het punt - we hebben het hier over slavernij. Zelfs als dat het geval is, is dat dan een logische reden om jezelf bewust tekort te doen?

“***Kwaliteit van leven is belangrijker dan alleen maar leven..***"  

Precies! Suggereer je dat de kwaliteit van leven van een verslaafde groter is dan die van iemand die niet verslaafd is? Geloof je echt dat de kwaliteit van leven van een gebruiker beter is dan die van een niet-gebruiker? Een leven dat wordt besteed aan het verbergen van de kop in het zand en ellendig zijn klinkt niet als een prettig leven.

“***Als je single bent en niet van plan bent om zich in de toekomst te vestigen, waarom dan niet?***"

Even als dat waar zou zijn, is dat dan een logische reden om te spelen met mechanismen voor neurologische impulsbeheersing? Kun je je voorstellen dat iemand dom genoeg is om zich bij elke gelegenheid uit te kleden, ongeacht hoe zeker ze zijn dat niemand het verwacht? **Dat is effectief wat pornogebruikers doen!**

De progressieve overstimulatie van onze beloningscircuits door overmatige stimulatie en het onvermogen om normale stressfactoren van het leven aan te kunnen, draagt niet bij aan het genieten van het leven met enthousiasme en energie. Porno en masturbatie hebben de natuurlijke seksuele eetlust vervangen, zoals een chocoladereep echt voedsel vervangt. Het is dan ook niet verwonderlijk dat veel artsen en psychologen nu verschillende geestelijke gezondheidsproblemen relateren aan fysiologische oorzaken. De reguliere medische gemeenschap heeft benadrukt dat pornografie nooit wetenschappelijk is bewezen als de directe oorzaak van de gemelde problemen door zelf bekennende personen. Echter, toegeven aan seksuele onmogelijkheid in het openbaar is zo'n schaamte-oproepende gebeurtenis, dus waarom zou iemand dit doen tenzij ze echt bezorgd waren, en de oorzaak hadden gevonden en uit hun eigen leven hadden verwijderd?

EasyPeasy zal je helpen om jezelf te bevrijden van porno en een gelukkige ex-gebruiker te worden. Geen porno, door porno gestimuleerde masturbatie of onnodige orgasmes meer. De enige hulp zal zijn de aanraking, het zicht en de geur van je partner. Als volkorenbrood na een goed ontwikkelde eetlust, zul je niet langer verlangen naar de high-fructose maïsstroop van internetporno. Het bewijs is zo overweldigend dat het geen bewijs nodig heeft; wanneer ik mijn duim met een hamer sla en het doet pijn, hoeft dat niet bewezen te worden. De stress van internetporno heeft doorvloeiende effecten op andere aspecten van het leven van de gebruiker, waardoor velen geneigd zijn hun toevlucht te nemen tot drugs zoals sigaretten en alcohol om ermee om te gaan, en in sommige gevallen zelfs de gastheer ertoe aanzetten zelfmoord te overwegen.

Gebruikers hebben ook illusies dat de negatieve effecten van porno worden overdreven. Het tegenovergestelde is het geval, er is geen twijfel dat internetporno de belangrijkste oorzaak is van seksuele disfunctie en vele andere problemen. Hoeveel echtscheidingen zijn veroorzaakt door porno? Er zijn geen betrouwbare manieren om dat te weten, maar zoekopdrachten in online gemeenschappen suggereren dat het aantal exponentieel groeit.

Er is een aflevering van *Friends* waarin de mannen, die continu gratis porno op tv ontvingen, begonnen zich af te vragen waarom de pizzabezorgster niet vroeg om hun 'grote slaapkamer' te bekijken. Als je verslaafd bent, projecteer je onvermijdelijk pornofantasieën op echte vrouwen. Stel je voor wat zorgeloze of zelfs accidentele blootstelling aan porno op de donkerdere kanten van het internet zou kunnen doen met iemand die al op een kantelpunt in zijn leven staat. Vechten tegen deze door porno veroorzaakte gedachten zal een grote aanslag zijn op hun geestelijke gezondheid.

(Hier is nog een gedachte-experiment: stel dat iemand naar je toe komt en zegt dat ze niet per se een orgasme willen, maar wel heel graag willen vrijen, zelfs penetratief. Ze willen het doen zo lang en ver als je kunt zonder een orgasme - maar als het gebeurt, is dat prima. Ik verzeker je van een fenomenale nieuwe seksuele ervaring, veel beter dan alle andere, als je ooit dat aanbod krijgt. Probeer het eens.)

De effecten van de hersenspoeling zorgen ervoor dat we neigen te denken als de man die van een gebouw van 100 verdiepingen valt en wordt geciteerd terwijl hij langs de vijftigste verdieping suist: "*Tot nu toe gaat het goed!*". We denken dat omdat we er tot nu toe mee weg zijn gekomen, nog één pornosessie geen verschil zal maken. Bekijk het eens anders: de 'gewoonte' is een reeks voor het leven, waarbij elke sessie de behoefte aan de volgende creëert. Wanneer je de gewoonte begint, steek je een lont aan. Het probleem is dat je niet weet hoe lang de lont is. Elke keer dat je toegeeft aan een pornosessie, ben je een stap dichter bij de bom die ontploft. **HOE WEET JE OF HET DE VOLGENDE KEER IS?**



## Donkere zwarte schaduwen

Users vinden het erg moeilijk te geloven dat internetporno daadwerkelijk die onzekere gevoelens veroorzaakt wanneer je laat op de avond buiten bent na een bewogen dag thuis of op het werk. Niet-gebruikers lijden niet aan dat gevoel, het is porno die het veroorzaakt.

Nog een van de grote geneugten van het stoppen met porno is de vrijheid van de duistere zwarte schaduwen achter in onze gedachten. Alle gebruikers weten dat ze dwaas zijn om hun geest af te sluiten voor de negatieve effecten van porno. Voor het grootste deel van ons leven gebeurt dit automatisch, maar de zwarte schaduwen loeren altijd in ons onderbewustzijn, net onder het oppervlak. Verscheidene van de fantastische voordelen van stoppen zijn bewust, zoals het beëindigen van de verspilling van tijd en van de pure domheid van het vrijen met een tweedimensionaal beeld.

De laatste hoofdstukken hebben de aanzienlijke voordelen behandeld van het niet gebruiken, maar in het belang van eerlijkheid is het noodzakelijk om een gebalanceerd beeld te geven. Daarom zal het volgende hoofdstuk de voordelen behandelen van het zijn van een gebruiker.

# Voordelen van het gebruik van porno.

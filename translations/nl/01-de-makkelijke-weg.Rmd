# De makkelijke manier

Het doel van dit boek is om je naar een nieuwe manier van denken te leiden. In tegenstelling tot de gebruikelijke methode om te stoppen - waarbij je begint met het gevoel alsof je de Mount Everest beklimt en de komende weken verlangt en je beroofd voelt - begin je meteen met een gevoel van vreugde, alsof je genezen bent van een vreselijke ziekte. Vanaf dat moment, hoe verder je door het leven gaat, hoe meer je terugkijkt op deze periode en je afvraagt hoe je ooit pornografie hebt gebruikt. Je zult andere gebruikers van pornografie met medelijden bekijken, in plaats van met jaloezie.

Tenzij je iemand bent die nooit verslaafd is geraakt (je leest dit voor je partner) of gestopt is (of zich in de vastendagen van een "pornodieet" bevindt), is het essentieel om door te blijven gaan met het gebruik van pornografie totdat je het boek volledig hebt uitgelezen. Dit lijkt misschien tegenstrijdig, en deze instructie om door te blijven gaan met masturberen naar pornografie roept meer bezwaar op dan enige andere, maar naarmate je verder leest, zal je verlangen om pornografie te gebruiken geleidelijk verminderen. **Neem deze instructie serieus: een vroegtijdige stopzetting zal je niet ten goede komen.**

Velen maken het boek niet af omdat ze het gevoel hebben iets op te moeten geven, sommigen lezen zelfs opzettelijk maar één regel per dag om het kwaad uit te stellen. Bekijk het eens zo: wat heb je te verliezen? Als je niet stopt aan het einde van het boek, ben je niet slechter af dan nu. Het is per definitie een Pascal's Wager, een gok waarbij je niets te verliezen hebt en een grote kans op grote winst.

Overigens, als je de afgelopen dagen of weken geen pornografie hebt gekeken, maar niet zeker weet of je een pornogebruiker, ex-gebruiker of niet-gebruiker bent, gebruik dan geen pornografie om te masturberen terwijl je leest. In feite ben je al een niet-gebruiker, maar we moeten je brein de kans geven om bij te blijven met je lichaam. Tegen het einde van het boek zul je een gelukkige niet-gebruiker zijn. EasyPeasy is het tegenovergestelde van de normale methode, waarbij men de aanzienlijke nadelen van pornografie opsomt en zegt:

*“Als ik maar lang genoeg zonder pornografie kan, zal uiteindelijk het verlangen verdwijnen en kan ik weer van het leven genieten, vrij van slavernij.”*  

Dit is de logische manier om het aan te pakken, met duizenden die elke dag stoppen met behulp van deze methode. Toch is het erg moeilijk om te slagen om de volgende redenen:

** Stoppen met PMO is niet het echte probleem.**

Elke keer dat je je sessie beëindigt, ben je gestopt met gebruiken. Je hebt misschien krachtige redenen op de eerste dag van je eens-in-de-vier-dagen-pornodieet om te zeggen: *"Ik wil geen porno meer gebruiken, of zelfs niet meer masturberen."* Alle gebruikers hebben dat, en hun redenen zijn krachtiger dan je je kunt voorstellen. Het echte probleem is dag twee, tien of tienduizend, waarop je in een zwak moment 'slechts één blik' werpt, er nog een wilt, en plotseling ben je weer verslaafd.

** Bewustzijn van de gezondheidsrisico's veroorzaakt meer angst, waardoor het moeilijker wordt om te stoppen.**

Zeg tegen een gebruiker dat het hun kracht vernietigt, en het eerste wat ze zullen doen is naar iets grijpen om hun dopamine te verhogen: een sigaret, alcohol, of zelfs de browser opstarten om naar porno te zoeken.

** Alle redenen om te stoppen maken het eigenlijk moeilijker.**

Dit komt door twee redenen. Ten eerste worden we voortdurend gedwongen om afscheid te nemen van onze 'kleine vriend' of een of ander hulpmiddel, verslaving of plezier (zoals de gebruiker het ook maar ziet). Ten tweede creëren ze een "blindheid". We masturberen niet om de redenen waarom we zouden moeten stoppen. De echte vraag is: waarom willen of moeten we het doen?

Met EasyPeasy vergeten we (in eerste instantie) de redenen waarom we willen stoppen, confronteren we het pornoprobleem en stellen we onszelf de volgende vragen:

1.  Wat doet porno voor mij?

2.  Geniet ik er eigenlijk van?

3.  Moet ik echt mijn leven doorbrengen door mijn geest en lichaam te saboteren?

De prachtige waarheid is dat *alle porno* absoluut niets voor je doet, helemaal niets. Laat me het heel duidelijk maken, het is niet zo dat de nadelen van het een gebruiker zijn opwegen tegen de voordelen, het is dat er ***geen enkel*** voordeel is aan het kijken naar porno.
De meeste gebruikers vinden het nodig om het goed te praten waarom ze porno gebruiken, maar de redenen die ze bedenken zijn allemaal onjuiste argumenten en illusies..

Allereerst zullen we deze onjuiste argumenten en illusies verwijderen. Sterker nog, je zult al snel realiseren dat er niets is om op te geven. Niet alleen dat, maar er zijn prachtige, positieve voordelen aan het zijn van een niet-PMO'er, met welzijn en geluk slechts twee van deze voordelen. Zodra de illusie dat het leven nooit zo plezierig zal zijn zonder porno wordt weggenomen - inzien dat het leven net zo plezierig is zonder, maar oneindig meer - en zodra gevoelens van tekortkoming of iets missen zijn uitgeroeid, zullen we terugkeren om te overwegen de toegenomen welzijn en geluk - en de tientallen andere redenen om te stoppen met porno. Deze realisaties zullen positieve aanvullende hulpmiddelen worden om je te helpen te bereiken wat je echt verlangt: genieten van je leven zonder de slavernij van pornoverslaving!
